
run: all
	./main.native

all:
	ocamlbuild -cflags -I,+lablgl -lflags -I,+lablgl -libs str,unix,lablgl,lablglut main.native

clean:
	ocamlbuild -clean
