
let pi2 = 6.2831853


(* draws the specified cylinder along the z axis.
 * explanation of how this works can be found at the bottom of this file *)
let cylinder ~numl ~numa ~length ~rmod ~rmodd ~xmod ~xmodd ~ymod ~ymodd =

  (* create static C arrays for the normals and vertices
   * (I would have used an interleaved array if lablgl had supported that) *)
  let normals  = Raw.create_static `float ((numl+1) * numa * 3) in
  let vertices = Raw.create_static `float ((numl+1) * numa * 3) in
  let idx l a = l*numa + a in

  (* calculate and fill the arrays *)
  for li = 0 to numl do
    let l = (float li) /. (float numl) *. length in
    let r = rmod l and rd = rmodd l in
    let x = xmod l and xd = xmodd l in
    let y = ymod l and yd = ymodd l in
    for ai = 0 to numa-1 do
      let a = pi2 *. (float (ai mod numa)) /. (float numa) in
      (* normal *)
      let nx = sin a *. r in
      let ny = cos a *. r in
      let nz = (sin a *. rd +. xd) *. -1.0 *. sin a *. r
             -. cos a *. r *. (cos a *. rd +. yd) in
      let len = sqrt (nx*.nx +. ny*.ny +. nz*.nz) in
      Raw.sets_float normals ~pos:(3*idx li ai) [| nx/.len; ny/.len; nz/.len |];
      (* vertex *)
      Raw.sets_float vertices ~pos:(3*idx li ai) [|
        sin a *. r +. x;
        cos a *. r +. y;
        l
      |];
    done;
  done;

  GlArray.enable `normal;
  GlArray.normal normals;
  GlArray.enable `vertex;
  GlArray.vertex `three vertices;

  (* draw the array *)
  GlDraw.begins `quads;
  for li = 1 to numl do
    for ai = 1 to numa do
      let a = ai mod numa in
      GlArray.element (idx (li-1)  a);
      GlArray.element (idx (li-1) (a-1));
      GlArray.element (idx  li    (a-1));
      GlArray.element (idx  li     a);
    done;
  done;
  GlDraw.ends ();

  (* I'd love to use gl[Push|pop]ClientAttrib(), but lablgl doesn't have that
   * function *)
  GlArray.disable `vertex;
  GlArray.disable `normal;

  Raw.free_static normals;
  Raw.free_static vertices



let draw t =
  Gl.enable `lighting;
  GlMat.push ();
  GlMat.translate3 (-4.5, 0.0, -4.0);
  GlMat.rotate ~angle:90.0 ~y:1.0 ();

  GlDraw.color (0.2, 0.2, 0.6);
  GlLight.material ~face:`both (`specular (0.1, 0.1, 0.1, 1.0));

  cylinder ~numl:45 ~numa:20 ~length:9.0
    ~rmod: (fun l -> 0.75 +. 0.5*.sin (l *. pi2 *. 0.2 +. t))
    ~rmodd:(fun l -> 0.5 *. cos (l *. pi2 *. 0.2 +. t) *. pi2 *. 0.2)
    ~xmod: (fun l -> 0.0)
    ~xmodd:(fun l -> 0.0)
    ~ymod: (fun l -> 0.5 *. sin (l *. pi2 *. 0.15 +. t))
    ~ymodd:(fun l -> 0.5 *. cos (l *. pi2 *. 0.15 +. t) *. pi2 *. 0.15);

  GlMat.pop ()


(*
  a = angle of the circle
  l = length of the cylinder

  rmod(l) = function returning the radius of the cylinder at the given length
  xmod(l)
  ymod(l) = functions returning the x and y positions of the center of the circle
            at a given length in the cylinder

  Every point on the cylinder can be described with the following formula:
  V(a,l) = [
    x = sin(a) * rmod(l) + xmod(l)
    y = cos(a) * rmod(l) + ymod(l)
          |        |          \--  translating the circle
          |        \-- scaling the circle
          \-- formula for a normal circle
    z = l
  ]

  (z could also be modulated to create more complex models, but let's keep
  things simple for now)

  This is an "Analytic Surface", as explained in http://glprogramming.com/red/appendixe.html
  So the normal can be calculated with:
  dV/dl -> l = [
    x = sin(a) * rmod'(l) + xmod'(l)
    y = cos(a) * rmod'(l) + ymod'(l)
    z = 1
  ]
  dV/da -> a = [
    x = cos(a) * rmod(l),
    y = -sin(a) * rmod(l),
    z = 0
  ]
  normal = [
    x = l.y * a.z - a.y * l.z = sin(a) * rmod(l)
    y = a.x * l.z - l.x * a.z = cos(a) * rmod(l)
    z = l.x * a.y - a.x * l.y
      = (sin(a) * rmod'(l) + xmod'(l)) * -sin(a) * rmod(l)
      - cos(a) * rmod(l) * (cos(a) * rmod'(l) + ymod'(l))
  ]
*)
