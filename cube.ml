(* A simple rotating cube, used for testing the .obj file loader and OpenGL shading *)

let dl_id = ref None

let load () =
  match !dl_id with
    Some i -> i
  | None ->
      let i = GlList.create `compile in
      Util.loadObj "data/cube.obj";
      GlList.ends ();
      dl_id := Some i;
      i


let draw t =
  let id = load () in
  Gl.enable `lighting;
  GlMat.push ();
  GlMat.translate ~z:(-4.0) ();
  GlMat.rotate3 ~angle:(t*.45.0) ((0.5+.0.5*.sin t), (0.5+.0.5*.cos (0.5*.t)), (0.5+.0.5*.sin (0.24*.t)));
  GlDraw.color (1.0, 0.0, 0.0);
  GlList.call id;
  GlMat.pop ()


