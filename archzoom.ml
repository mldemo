(* Very lame and badly implemented infinite zoom on an Arch Linux logo,
 * loaded to texture from data/arch.bmp *)



let tex_id = ref None


let settex () =
  match !tex_id with
    Some n -> GlTex.bind_texture `texture_2d n
  | None   -> tex_id := Some (Util.setTexture (Util.loadBMP "data/arch.bmp"))



(* this zoom function assumes the Arch texture is activated *)
let draw t =
  settex ();
  (* this code doesn't function with cull_face and depth_test enabled *)
  Gl.disable `cull_face;
  Gl.disable `depth_test;
  Gl.disable `lighting;
  GlMat.push ();
  Gl.enable `texture_2d;
  let d () =
    GlDraw.begins `quads;
    GlTex.coord2( 0.0,  1.0); GlDraw.vertex3 (-1.0,  1.0, 0.0);
    GlTex.coord2( 1.0,  1.0); GlDraw.vertex3 ( 1.0,  1.0, 0.0);
    GlTex.coord2( 1.0,  0.0); GlDraw.vertex3 ( 1.0, -1.0, 0.0);
    GlTex.coord2( 0.0,  0.0); GlDraw.vertex3 (-1.0, -1.0, 0.0);
    GlDraw.ends () in
  let di = 1.05 in
  let ti = 2.80 +. t -. (float (truncate (t/.di)))*.di in
  let zoom = ti**ti in
  GlMat.scale3 (zoom, zoom, 1.0);
  GlMat.translate3 (0.0, 0.33333, -2.0);
  for i=0 to 3 do
    GlMat.translate ~y:(-0.3) ();
    GlMat.scale3 (0.1, 0.1, 1.0);
    d ();
  done;
  Gl.disable `texture_2d;
  GlMat.pop ();
  (* re-enable cull_face and depth_test, as the other effects assume these to be
   * enabled *)
  Gl.enable `cull_face;
  Gl.enable `depth_test
