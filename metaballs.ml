(* Very basic and naive implementation of software-rendered 2D metaballs on a texture *)



(* texture on which we're drawing the metaballs *)
let tex = GlPix.create `float ~format:`alpha ~width:128 ~height:128

(* OpenGL texture ID *)
let tex_id = ref None


(* Set active texture to tex_id, or obtain a new ID
 * if we haven't allocated one yet *)
let settex () =
  match !tex_id with
    Some n -> GlTex.bind_texture `texture_2d n
  | None   -> tex_id := Some (Util.setTexture tex)



(* calculates metaballs and saves the result in the currently active texture
 * http://www.gamedev.net/reference/programming/features/isometa2d/ *)
let calc t =
  let raw = GlPix.to_raw tex in
  let pos = GlPix.raw_pos tex in
  let h = GlPix.height tex in
  let w = GlPix.width tex in
  (* ball coordinates *)
  let b1x = 0.47+.0.1 *.cos (t*.1.5) and b1y = 0.56+.0.03*.sin (t*.2.0) in
  let b2x = 0.52+.0.21*.sin t        and b2y = 0.55+.0.07*.cos t in
  let b3x = 0.47+.0.2 *.sin (t*.1.3) and b3y = 0.45+.0.15*.cos (2.0*.cos t) in
  (* ball equation *)
  let ball x y r px py = r /. ((x-.px)**2.0 +. (y-.py)**2.0) in
  (* calculate pixel values *)
  for y = 0 to h-1 do
    for x = 0 to w-1 do
      let px = ((float x)/.(float w)) and py = ((float y)/.(float h)) in
      let c = 1.3 -. (
              (ball px py 0.003 0.5 0.5) +.
              (ball px py 0.004 b1x b1y) +.
              (ball px py 0.007 b2x b2y) +.
              (ball px py 0.002 b3x b3y)) in
      Raw.set_float raw ~pos:(pos ~x:x ~y:y) c;
    done;
  done;
  (* GL_ALPHA8 = 32828;  GL_ALPHA16 = 32830 *)
  GlTex.image2d ~internal:32828 tex



(* draw the metabals on the screen *)
let draw t =
  settex ();
  calc t;
  Gl.disable `lighting;
  Gl.enable `texture_2d;
  Util.setOrtho 1.0 1.0;
  GlDraw.color ~alpha:1.0 (0.0, 0.0, 0.0);
  GlDraw.begins `quads;
  GlTex.coord2 (0.0, 0.0); GlDraw.vertex3 (0.0, 0.0, 0.0);
  GlTex.coord2 (1.0, 0.0); GlDraw.vertex3 (1.0, 0.0, 0.0);
  GlTex.coord2 (1.0, 1.0); GlDraw.vertex3 (1.0, 1.0, 0.0);
  GlTex.coord2 (0.0, 1.0); GlDraw.vertex3 (0.0, 1.0, 0.0);
  GlDraw.ends ();
  Util.setPerspective ();
  Gl.disable `texture_2d;

