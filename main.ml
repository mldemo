

(* window dimensions, as float because it's mostly needed as such
 * (float <-> int conversions are expensive and annoying to type) *)
let width = 960.0 and height = 540.0
let starttime = Unix.gettimeofday ()



let drawFrame () =
  let t = (Unix.gettimeofday ()) -. starttime in
  GlClear.clear [`color; `depth];
  GlMat.load_identity ();
  (*Archzoom.draw t;
  Cube.draw t;*)
  Cylindermod.draw t;
  (*Metaballs.draw t;*)
  Util.drawInfo height width t;
  Glut.swapBuffers ()



let _ =
  (* init glut *)
  ignore (Glut.init Sys.argv);
  Glut.initDisplayMode ~alpha:true ~depth:true ~double_buffer:true ~multisample:true ();
  Glut.initWindowSize (truncate width) (truncate height);
  ignore (Glut.createWindow "De Neuspeuteraars!");
  Glut.idleFunc (Some drawFrame);
  Glut.keyboardFunc (fun ~key ~x ~y ->
    Printf.printf "Exited after %.3fs\n" ((Unix.gettimeofday ())-.starttime);
    flush stdout;
    exit 0
  );
  (* init gl *)
  GlClear.color (0.0, 0.0, 0.0);
  GlClear.depth 1.0;
  Gl.enable `blend;
  GlFunc.blend_func `src_alpha `one_minus_src_alpha;

  (* Some nice articles about lighting:
   * http://www.sjbaker.org/steve/omniv/opengl_lighting.html
   * http://www.spacesimulator.net/tut5_vectors_and_lighting.html
   * http://www.falloutsoftware.com/tutorials/gl/gl8.htm *)
  GlDraw.shade_model `smooth;
  Gl.enable `light0;
  GlLight.light 0 (`position (1.0, 1.0, 0.0, 1.0));
  GlLight.light 0 (`ambient  (0.0, 0.0, 0.0, 1.0));
  GlLight.light 0 (`diffuse  (0.8, 0.8, 0.8, 1.0));
  GlLight.light 0 (`specular (0.7, 0.7, 0.7, 1.0));
  GlLight.light_model (`ambient (0.2, 0.2, 0.2, 1.0));
  Gl.enable `color_material;
  GlLight.color_material ~face:`both `ambient_and_diffuse;
  GlLight.material ~face:`both (`specular (1.0, 1.0, 1.0, 1.0));
  GlLight.material ~face:`both (`shininess 50.0);
  GlLight.material ~face:`both (`emission (0.0, 0.0, 0.0, 1.0));

  GlTex.env (`mode `modulate);
  Gl.enable `depth_test;
  Gl.enable `cull_face;
  GlMat.mode `projection;
  GlMat.load_identity ();
  GluMat.perspective ~fovy:60.0 ~aspect:(width /. height) ~z:(0.1,100.0);
  GlDraw.viewport 0 0 (truncate width) (truncate height);
  GlMat.mode `modelview;
  (* run *)
  Glut.mainLoop ()

